using System;

namespace TestCore.Web.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId { get => !string.IsNullOrEmpty(RequestId); }
    }
}